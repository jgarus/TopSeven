# TopSeven

# Prerequisites
Currently need to clone the Volley framework (into root directory /TopSeven) then simply sync gradle.

`git clone https://android.googlesource.com/platform/frameworks/volley`

OR this one `git clone https://github.com/google/volley.git` which Google seems to merged to GitHub
Source page also updated to show the GitHub repo, but they both work. 

Google source here: https://developer.android.com/training/volley/index.html

NOTE: On Linux, Volley MIGHT need few extra files if build fails.
`sudo apt install -y lib32gcc1 libc6-i386 lib32z1 lib32stdc++6
 sudo apt install -y lib32ncurses5 lib32gomp1 lib32z1-dev lib32bz2-dev`


# API Reference
The API used to parse the data was put together using Ruby along with Sinatra.  The app is hosted on Heroku.
https://topseven7.herokuapp.com/ 

And the link to the API repo https://github.com/jgarus/topseven7_api


# Author
Jesus Garcia
javprg7@gmail.com
