import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ListView;

import com.example.jesus.topseven.Club;
import com.example.jesus.topseven.ClubAdapter;
import com.example.jesus.topseven.ClubsActivity;
import com.example.jesus.topseven.R;
import com.example.jesus.topseven.Util;

import org.json.JSONException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;

/**
 * Since I wanted to learn more about Volley, this will AsyncTask will not be used.
 * Volley will handle the ClubsActivity while AsyncTask will handle the LeaguesActivity.
 *
 * This is where we will handle HTTP call and populate the ListView on PostExecute
 *
 * Created by jesus on 3/26/16.
 */

//Get string values and return ArrayList of Club objects
public class ClubAsyncTask extends AsyncTask<String, Void, ArrayList<Club>> {

    ClubsActivity activity;

    //Our API endpoint
    //String endpoint = "https://topseven7.herokuapp.com/clubs/ligue_1";

    //Progress dialog to show when loading data
    ProgressDialog progressDialog;

    public ClubAsyncTask(ClubsActivity clubsActivity) {
        this.activity = clubsActivity;
    }



    @Override
    protected ArrayList<Club> doInBackground(String... params) {
        try {

           // endpoint.replace()

            String endpoint = params[0];
            Log.d("d", "GIVEN ENDPOINT FROM CLUB ACTIVITY?: " + params[0]);

            //Our URL to retrieve data from
            URL url = new URL(endpoint);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();

            connection.setRequestMethod("GET");
            connection.connect();
            int statusCode = connection.getResponseCode(); //Connection status code

            if (statusCode == HttpURLConnection.HTTP_OK) {
                BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                StringBuilder builder = new StringBuilder();

                String line;

                //appends the JSON data, queries and updates the builder
                while ((line = reader.readLine()) != null) {
                    builder.append(line);
                }

                return Util.JsonParser.parseClubs(builder.toString());
            }

        } catch (MalformedURLException e) {
            e.getCause();
        }  catch (IOException e) {
            e.printStackTrace();
            return null;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }

        return null;
    }

    //We will have a progress dialog here
    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progressDialog = new ProgressDialog(activity);
        progressDialog.setMessage("Loading Teams");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }


    //Where we will populate the listView
    @Override
    protected void onPostExecute(ArrayList result) {
        super.onPostExecute(result);
        progressDialog.dismiss();
        ListView clubListView = (ListView) activity.findViewById(R.id.club_list_view);
        final ClubAdapter adapter = new ClubAdapter(activity, R.layout.club_row_layout, result);
        adapter.notifyDataSetChanged();
        clubListView.setAdapter(adapter);
    }
}