package com.example.jesus.topseven;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by jesus on 9/20/16.
 */
public class PlayerAdapter extends ArrayAdapter<Player> {
    private Context context;

    public  PlayerAdapter(Context context, int resource, List<Player> players){
        super(context, resource, players);
        this.context = context;
    }

    @Override
    @NonNull
    public View getView(int position, View convertView, ViewGroup parent) {
        Player player = getItem(position);

        //Inflate the view if it isn't being used
        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.player_row_layout,
                    parent, false);


            //Instantiate TextView and Set player name
            TextView playerName = (TextView) convertView.findViewById(R.id.player_name);
            playerName.setText(player.getPlayerName());

            //Instantiate TextView and set player position
            TextView playerPosition = (TextView) convertView.findViewById(R.id.player_position);
            playerPosition.setText(player.getPosition());

            //Instantiate ImageView and set image using Picasso
            ImageView playerImage = (ImageView) convertView.findViewById(R.id.player_image);

            Picasso.with(context).setIndicatorsEnabled(true);
            Picasso.with(context).load(player.getPlayerPicture()).resize(150,150)
                    .centerCrop().
                    into(playerImage);

        }
        return convertView;
    }
}
