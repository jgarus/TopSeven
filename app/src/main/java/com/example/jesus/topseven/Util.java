package com.example.jesus.topseven;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Create our JSON request here. The only one we will use is the
 * parseLeagues since the parseClubs will be handled by Volley in
 * the ClubsActivity, this way I get to use both Async and Volley
 *
 * Created by jesus on 3/26/16.
 */
public class Util {
    static public class JsonParser{

        //Parse the JSON leagues
        static ArrayList<League> parseLeagues(String result) throws JSONException{
            Log.d("d", "Printing Result in Util: " + result);

            JSONObject jsonObject = new JSONObject(result);
            JSONArray leagues = jsonObject.getJSONArray("leagues");
            ArrayList<League> leagues_array = new ArrayList<>();

            JSONObject leagueLogo;
            JSONObject countryFlag;

            for(int i = 0; i < leagues.length(); i++){
                JSONObject leaguesJSON = leagues.getJSONObject(i);

                League league = new League();
                league.setId(leaguesJSON.getString("leagueID"));
                league.setLeagueName(leaguesJSON.getString("leagueName"));
                league.setCountry(leaguesJSON.getString("country"));

                leagueLogo = leagues.getJSONObject(i);
                league.setLeagueLogo(leagueLogo.getString("leagueLogo"));
                Log.d("d", "League Logo URL: " + league.getLeagueLogo());


                countryFlag = leagues.getJSONObject(i);
                league.setCountryFlag(countryFlag.getString("countryFlag"));
                Log.d("d", "Country flag URL: " + league.getCountryFlag());

                leagues_array.add(league);

            }
            return leagues_array;

        }



        //Parsing Clubs
        static public ArrayList<Club> parseClubs(String result) throws JSONException {

            JSONObject jsonObject = new JSONObject(result);
            JSONArray clubs = jsonObject.getJSONArray("teams");
            ArrayList<Club> clubs_array = new ArrayList<>();

            JSONObject clubCrest;

            for(int i = 0; i < clubs.length(); i++){
                JSONObject clubsJSON = clubs.getJSONObject(i);

                Club club = new Club();
                club.setCode(clubsJSON.getString("clubCode"));
                club.setName(clubsJSON.getString("clubName"));
                club.setShortName(clubsJSON.getString("shortClubName"));
                club.setSquadValue(clubsJSON.getString("marketValue"));

                clubCrest = clubs.getJSONObject(i);
                club.setCrestURl(clubCrest.getString("crestUrl"));

                clubs_array.add(club);
            }
            return clubs_array;
        }


    }//End JSONParser
}
