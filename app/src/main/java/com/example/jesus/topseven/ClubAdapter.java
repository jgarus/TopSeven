package com.example.jesus.topseven;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Adapter to load the ListView with all the necessary information
 * i.e. Images, names, etc...
 *
 * I decided to use Picasso to load my images.
 *
 * Created by jesus on 3/26/16.
 */
public class ClubAdapter extends ArrayAdapter<Club>{
    private Context context;

    public ClubAdapter(Context context, int resource, List<Club> clubs) {
        super(context, resource, clubs);
        this.context = context;
    }

    //Get the view used as row in certain position in list view.
    @Override
    @NonNull
    public View getView(int position, View convertView, ViewGroup parent) {
        //Get data for this position
        Club club = getItem(position);

        //If view is not being used, then inflate the view
        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.club_row_layout,parent, false);
        }

        //Setting the name to each club in the ListView
        TextView club_name = (TextView) convertView.findViewById(R.id.club_name);
        club_name.setText(club.getShortName());
//        TextView market_val = (TextView) convertView.findViewById(R.id.market_value);
//        market_val.setText("Mkt. Value: " + club.getSquadValue());
        TextView club_position = (TextView) convertView.findViewById(R.id.position);
        club_position.setText(club.getPosition());
        TextView points = (TextView) convertView.findViewById(R.id.points);
        points.setText(club.getPoints());

        //Using picasso to load image into imageView
        ImageView crest = (ImageView) convertView.findViewById(R.id.club_crest);
        Picasso.with(context).load(club.getCrestURl()).resize(150,150).centerCrop().into(crest);
        return convertView;
    }
}
