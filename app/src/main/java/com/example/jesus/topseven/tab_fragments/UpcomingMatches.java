package com.example.jesus.topseven.tab_fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.example.jesus.topseven.R;

/**
 * Created by jesus on 8/9/16.
 */
public class UpcomingMatches extends Fragment {


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.tab_upcoming_matches, container, false);
    }

}
