package com.example.jesus.topseven;

import android.content.Intent;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

/*
    Created by jesus on 7/15/16.
 */


public class LeaguesActivity extends AppCompatActivity  {

    DrawerLayout drawerLayout;
    private static String message;
    public final static String EXTRA_MESSAGE = message;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leagues);

        //Initializing the toolbar view
        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);

        drawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.app_name, R.string.app_name);
        drawerLayout.addDrawerListener(drawerToggle);
        //Prepare command, ready to go
        drawerToggle.syncState();

        new LeagueAsyncTask(LeaguesActivity.this).execute();


        //Set a ClickListener for when you click an item on the ListView
        ((ListView)findViewById(R.id.league_list_view)).setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                TextView textView = (TextView) view.findViewById(R.id.league_name);
                message = textView.getText().toString();

                Intent intent;
                intent = new Intent(LeaguesActivity.this, ClubsActivity.class);
                intent.putExtra(EXTRA_MESSAGE, message);
                startActivity(intent);
            }
        });
    }
}
