package com.example.jesus.topseven;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by jesus on 7/15/16.
 *
 * This adapter will load the List View with the data gathered from the League class
 */
class LeagueAdapter extends ArrayAdapter<League> {
    Context context;

    //Default constructor
    LeagueAdapter(Context context, int resource, List<League> leagueList){
        super(context,resource,leagueList);
        this.context = context;
    }

    @Override
    @NonNull
    public View getView(int position, View convertView, ViewGroup parent) {
        //Get data for this position in the ListView
        League league = getItem(position);

        //If the view isn't being used, then inflate view
        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.league_row_layout,parent,false);
        }

        //Setting the name to each club in the ListView
        TextView league_name = (TextView) convertView.findViewById(R.id.league_name);
        league_name.setText(league.getLeagueName());

        //Using picasso to load image into imageView
        //Center inside does the job well, scales images so both dimensions are euqal to or less than
        //the requested ImageView bounds
        ImageView league_logo = (ImageView) convertView.findViewById(R.id.league_logo);
        ImageView country_flag = (ImageView) convertView.findViewById(R.id.country_flag);
        Picasso.with(context).load(league.getLeagueLogo()).resize(150,250).centerInside().into(league_logo);
        Picasso.with(context).load(league.getCountryFlag()).resize(75,75).centerInside().into(country_flag);
        return convertView;



    }
}
