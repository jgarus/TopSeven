package com.example.jesus.topseven.tab_fragments;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

/**
 * Created by jesus on 8/9/16.
 *
 * This adapter provides fragment views to tabs
 *
 * Extending a FragmentPagerAdapter because, according to Google, this saves us memory
 * by destroying and recreating the fragments as needed and by only saving the state.
 *
 */
public class ViewPagerAdapter extends FragmentStatePagerAdapter {

    //Store number of tabs
    private static final int TAB_COUNT = 3;

    //Creating our titles for the tabs
    private String tabTitles[] = new String[] { "Squad", "Table", "Upcoming" };

    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        //Returning the current tabs
        switch (position) {
            case 0:
                return new Squad();
            case 1:
                return new LastTable();
            case 2:
                return new UpcomingMatches();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return TAB_COUNT;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        return tabTitles[position].toLowerCase();
    }
}
