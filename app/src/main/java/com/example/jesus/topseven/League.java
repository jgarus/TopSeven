package com.example.jesus.topseven;

/**
 * Created by jesus on 3/24/16.
 */
class League {

    private String leagueName, country, leagueLogo, countryFlag, id;

    League() {
    }

    League(String leagueName, String country, String leagueLogo, String countryFlag, String id) {
        this.leagueName = leagueName;
        this.country = country;
        this.leagueLogo = leagueLogo;
        this.countryFlag = countryFlag;
        this.id = id;
    }


    String getLeagueName() {
        return leagueName;
    }

    void setLeagueName(String leagueName) {
        this.leagueName = leagueName;
    }

    String getCountry() {
        return country;
    }

    void setCountry(String country) {
        this.country = country;
    }

    String getLeagueLogo() {
        return leagueLogo;
    }

    void setLeagueLogo(String leagueLogo) {
        this.leagueLogo = leagueLogo;
    }

    String getCountryFlag() {
        return countryFlag;
    }

    void setCountryFlag(String countryFlag) {
        this.countryFlag = countryFlag;
    }

    String getId() {
        return id;
    }

    void setId(String id) {
        this.id = id;
    }

}
