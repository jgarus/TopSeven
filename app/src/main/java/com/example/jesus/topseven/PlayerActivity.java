package com.example.jesus.topseven;


import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import com.example.jesus.topseven.tab_fragments.ViewPagerAdapter;


/*
    Created by jesus on 8/5/16.
 */

public class PlayerActivity extends AppCompatActivity implements TabLayout.OnTabSelectedListener{

    //Give the tab count to the ViewPagerAdapter


    TabLayout tabLayout;
    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player);
        setToolbar();

        //Initializing ViewPager which should allow us to swipe from tab to tab
        viewPager = (ViewPager) findViewById(R.id.view_pager);
        //Setting Adapter to Pager so we ca display items
        viewPager.setAdapter(new ViewPagerAdapter(getSupportFragmentManager()));

        //Initializing the TabLayout
        tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        //Make tabs fill up as much space as possible
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        //Attaching the view pager to the tabs
        tabLayout.setupWithViewPager(viewPager);
    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        viewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {
    }

    public void setToolbar(){
        //Adding the toolbar to the activity
        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
    }
}
