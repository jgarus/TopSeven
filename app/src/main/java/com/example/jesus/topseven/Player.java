package com.example.jesus.topseven;

/**
 * Created by jesus on 3/25/16.
 *
 *
 */
public class Player {

    private String playerName, playerNumber, position,
            birthdate, age, birthDate
            , birthPlace, transferValue, playerPicture;


    public Player() {
    }

    String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    String getPlayerNumber() {
        return playerNumber;
    }

    public void setPlayerNumber(String playerNumber) {
        this.playerNumber = playerNumber;
    }

    String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    String getBirthPlace() {
        return birthPlace;
    }

    public void setBirthPlace(String birthPlace) {
        this.birthPlace = birthPlace;
    }

    String getTransferValue() {
        return transferValue;
    }

    public void setTransferValue(String transferValue) {
        this.transferValue = transferValue;
    }

    String getPlayerPicture() {
        return playerPicture;
    }

    public void setPlayerPicture(String playerPicture) {
        this.playerPicture = playerPicture;
    }
}
