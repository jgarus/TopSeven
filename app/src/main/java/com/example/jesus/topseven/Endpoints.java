package com.example.jesus.topseven;

/**
 * Created by jesus on 1/7/17.
 * <p>
 * Decided to organize all my API endpoints
 */

class Endpoints {

    //League URL
    static String ligue_1 = "https://topseven7.herokuapp.com/clubs/ligue_1",
            premier_league = "https://topseven7.herokuapp.com/clubs/premier",
            primeira_liga = "https://topseven7.herokuapp.com/clubs/primeira",
            la_liga = "https://topseven7.herokuapp.com/clubs/laliga",
            bundesliga = "https://topseven7.herokuapp.com/clubs/bundesliga",
            serie_a = "https://topseven7.herokuapp.com/clubs/serie_a",
            liga_mx = "https://topseven7.herokuapp.com/clubs/liga_mx";

    //Base URL. This will load all of the league main leagues
    //Names and Images will be loaded
    static String leagues_url = "https://topseven7.herokuapp.com/leagues";
}
