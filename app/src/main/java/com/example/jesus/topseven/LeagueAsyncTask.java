package com.example.jesus.topseven;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ListView;

import org.json.JSONException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

/**
 *
 * Created by jesus on 7/16/16.
 *
 */
public class LeagueAsyncTask extends AsyncTask<String, Void, ArrayList<League>> {

    private LeaguesActivity leaguesActivity;

    //A progress dialog to show progress
    private ProgressDialog progressDialog;

    //Constructor for LeaguesActivity
    LeagueAsyncTask(LeaguesActivity leaguesActivity) {
        this.leaguesActivity = leaguesActivity;
    }

    @Override
    protected ArrayList<League> doInBackground(String... params) {
        try{

            //URL comes from the Endpoints
            URL url = new URL(Endpoints.leagues_url);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();

            //request type
            connection.setRequestMethod("GET");
            //connect
            connection.connect();
            //get status code regarding connection
            int statusCode = connection.getResponseCode();

            //if everything is good (200):
            if (statusCode == HttpURLConnection.HTTP_OK){
                BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                StringBuilder builder = new StringBuilder();

                String line;

                //append JSON data, query and update the builder
                while ((line = reader.readLine()) != null){
                    builder.append(line);
                }

                Log.d("d", "PARSED LEAGUES IN ASYNCTASK:" + line);

                //return parsed JSON data
                return Util.JsonParser.parseLeagues(builder.toString());

            }
        }catch (MalformedURLException e){
            e.printStackTrace();
            return null;
        }catch (IOException e){
            e.printStackTrace();
            return null;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    //We will have a progress dialog here
    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progressDialog = new ProgressDialog(leaguesActivity);
        progressDialog.setMessage("Loading...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setIndeterminate(true);
        progressDialog.setMax(100);
        progressDialog.setCancelable(false);
        progressDialog.show();
        progressDialog.setProgress(5);
    }


    //Where we will set the adapter to the ListView
    @Override
    protected void onPostExecute(ArrayList<League> result) {
        super.onPostExecute(result);
        progressDialog.dismiss();
        ListView leagueListView = (ListView) leaguesActivity.findViewById(R.id.league_list_view);
        final LeagueAdapter adapter = new LeagueAdapter(leaguesActivity, R.layout.league_row_layout, result);
        adapter.notifyDataSetChanged();
        leagueListView.setAdapter(adapter);
    }
}
