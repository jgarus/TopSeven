package com.example.jesus.topseven.tab_fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.jesus.topseven.AppController;
import com.example.jesus.topseven.Player;
import com.example.jesus.topseven.PlayerAdapter;
import com.example.jesus.topseven.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


/**
 * Created by jesus on 8/9/16.
 *
 * Fragment in testing
 *
 */
public class Squad extends Fragment {

    private static final String TAG = Squad.class.getSimpleName();

    ArrayList<Player> squad_list;
    String url = "https://topseven7.herokuapp.com/players/laliga/villarreal";

    ListView squadListView;
    PlayerAdapter squadAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.tab_squad, container, false);

        squad_list = new ArrayList<>();

        //Initializing ListView and Adapter
        squadListView = (ListView) view.findViewById(R.id.list);
        squadAdapter = new PlayerAdapter(getActivity(), R.layout.player_row_layout, squad_list);

        squadListView.setAdapter(squadAdapter);
        fetchJSON();

        return view;
    }

    public void fetchJSON(){

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {


                            //JSON player array
                            JSONArray players = response.getJSONArray("players");


                            for (int i = 0; i < players.length(); i++) {
                                JSONObject jsonObject = (JSONObject) players.get(i);

                                Player player = new Player();

                                player.setPlayerName(jsonObject.getString("name"));
                                player.setPlayerPicture(jsonObject.getString("playerPicture"));
                                player.setPosition(jsonObject.getString("position"));

                                squad_list.add(player);

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        //ArrayList is now populated
                        squadAdapter.notifyDataSetChanged();
                        squadListView.setAdapter(squadAdapter);

                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });
        AppController.getInstance().addToRequestQueue(request, TAG);
    }
}
