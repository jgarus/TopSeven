package com.example.jesus.topseven;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;


/*
    Created by jesus on 6/26/16.
 */

public class ClubsActivity extends AppCompatActivity {

    private static final String TAG = ClubsActivity.class.getSimpleName();

    public String message;
    ProgressDialog progressDialog;

    ListView clubListView;
    ClubAdapter adapter;

    ArrayList<Club> clubs_list = new ArrayList<>();
    String url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_clubs);

        Intent intent = getIntent();
        message = intent.getStringExtra(LeaguesActivity.EXTRA_MESSAGE);
        Toast.makeText(ClubsActivity.this, message, Toast.LENGTH_SHORT).show();

        setListViewAdapter(); //Setting the ListView
        loadURL(); //Loading url based on league
        fetchJSON(); //Calling the JSON data


        //Set a ClickListener for when you click an item on the ListView
        ((ListView)findViewById(R.id.club_list_view)).setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {

                Intent intent;
                intent = new Intent(ClubsActivity.this, PlayerActivity.class);
                startActivity(intent);
            }
        });
    }


    //Creating our volley JSON request, it will be handled by the AppController Singleton
    public void fetchJSON(){
        startProgressDialog();
        final JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            JSONArray clubs = response.getJSONArray("clubs");


                            for (int i = 0; i < clubs.length(); i++) {
                                JSONObject jsonObject = (JSONObject) clubs.get(i);

                                Club club = new Club();

                                club.setCode(jsonObject.getString("clubCode"));
                                club.setName(jsonObject.getString("clubName"));
                                club.setShortName(jsonObject.getString("shortClubName"));
                                club.setSquadValue(jsonObject.getString("marketValue"));
                                club.setPoints(jsonObject.getString("points"));
                                club.setPosition(jsonObject.getString("position"));
                                club.setCrestURl(jsonObject.getString("crestUrl"));

                                clubs_list.add(club);
                                sortList(); //Calling the sort method

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        //ArrayList is now populated
                        adapter.notifyDataSetChanged();
                        clubListView.setAdapter(adapter);
                        dismissDialog();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //If the request comes up with no data, cancel requests
                        //Then dismiss the progress dialog
                        //Finally we're going to ask the user if they want to refresh.
                        AppController.getInstance().cancelPendingRequests(TAG);
                        dismissDialog();
                        onNullError();
                    }
                });
        //add to request queue
        AppController.getInstance().addToRequestQueue(request, TAG);
    }

    //change URL depending on league
    public String loadURL(){
        switch (message){
            case "Ligue 1": url = Endpoints.ligue_1;
                break;
            case "Premier League": url  = Endpoints.premier_league;
                break;
            case "Primeira Liga": url  = Endpoints.primeira_liga;
                break;
            case "LaLiga": url  = Endpoints.la_liga;
                break;
            case "Bundesliga": url  = Endpoints.bundesliga;
                break;
            case "Serie A": url  = Endpoints.serie_a;
                break;
            case "Liga MX": url  = Endpoints.liga_mx;
                break;
        }
        return url;
    }

    //Method for progress dialog start
    public void startProgressDialog(){
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    //Method for progress dialog dismiss
    public void dismissDialog(){
        progressDialog.dismiss();
    }

    //Initializing adapter and ListView
    public void setListViewAdapter(){
        clubListView = (ListView) ClubsActivity.this.findViewById(R.id.club_list_view);
        adapter = new ClubAdapter(ClubsActivity.this, R.layout.club_row_layout, clubs_list);
    }

    //Ask user if they want to refresh data if we get a null upon request
    public void onNullError(){
        AlertDialog.Builder builder = new AlertDialog.Builder(ClubsActivity.this);

        //dialog message and title
        builder.setMessage("Unable to fetch data...Refresh?").setTitle("Error");

        //Ok to refresh
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                Log.d("d", "CLICKING OK: ");
                //fetching data
                fetchJSON();
            }
        });

        //Cancel refresh and return
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                Log.d("d", "CLICKING CANCEL: ");
                //canceling dialog
                dialog.cancel();
                //calling finish will terminate this activity and return to LeaguesActivity
                finish();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    /*We will sort array here by points.
    * Since the table is ever changing after every game, It will sort the table by points
    * and then depending  on the points, the sorting will be done in either,
    * goal difference or goals for.
     */
    public void sortList(){
        Collections.sort(clubs_list, new Comparator<Club>() {
            @Override
            public int compare(Club club, Club t1) {
                try{
                    return t1.getPoints().compareTo(club.getPoints());
                }catch (Exception e){
                    e.printStackTrace();
                }
                return 0;
            }
        });
    }
}
