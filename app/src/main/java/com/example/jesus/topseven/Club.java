package com.example.jesus.topseven;

/**
 * Created by jesus on 3/26/16.
 */
public class Club {
    //int id;
    private String name, shortName, crestURl, code, squadValue, points, position;

    Club() {
    }

    Club(String name, String shortName, String crestURl, String code, String squadValue, String points, String position) {
        this.name = name;
        this.shortName = shortName;
        this.crestURl = crestURl;
        this.code = code;
        this.squadValue = squadValue;
        this.points = points;
        this.position = position;
    }

    String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    String getCrestURl() {
        return crestURl;
    }

    public void setCrestURl(String crestURl) {
        this.crestURl = crestURl;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getSquadValue() {
        return squadValue;
    }

    public void setSquadValue(String squadValue) {
        this.squadValue = squadValue;
    }

    String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

}
